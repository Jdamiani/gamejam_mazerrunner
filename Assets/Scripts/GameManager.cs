﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    GameObject GameOverScreen;

    [SerializeField]
    GameObject WinScreen;
    
    private void Start()
    {
        GameOverScreen.SetActive(false);
        WinScreen.SetActive(false);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameOverScreen.SetActive(true);
        }
    }
    
    public void RestartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
