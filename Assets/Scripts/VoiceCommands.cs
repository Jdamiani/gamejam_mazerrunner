﻿using System.Collections.Generic;
using UnityEngine.Windows.Speech;
using UnityEngine;
using System.Linq;
using System;



public class VoiceCommands : MonoBehaviour
{
[SerializeField]
private GameObject _player;

private KeywordRecognizer _recognizer;
private Dictionary<string, System.Action> _keywords = new Dictionary<string, System.Action>();




private void Start()
{
   if (_recognizer == null)
    {
    float _yRotationLeft = -90.0f;
    float _yRotationRight = 90.0f;
    float _yRotationFlip = 180.0f;
_keywords.Add("left", () =>
{
    transform.Rotate(0.0f, _yRotationLeft, 0.0f);
});
_keywords.Add("midleft", () =>
{
    transform.Rotate(0.0f, (_yRotationLeft/2), 0.0f);
});
_keywords.Add("right", () =>
{
    transform.Rotate(0.0f, _yRotationRight, 0.0f);
});
_keywords.Add("midright", () =>
{
    transform.Rotate(0.0f, (_yRotationRight/2), 0.0f);
});
_keywords.Add("back", () =>
{
    transform.Rotate(0.0f, _yRotationFlip, 0.0f);
});
_keywords.Add("go", () =>
{
    transform.position += transform.forward * 1;
});


    _recognizer = new KeywordRecognizer(_keywords.Keys.ToArray());
    _recognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
    _recognizer.Start();
    }
}

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action _keywordAction;
        if(_keywords.TryGetValue(args.text, out _keywordAction))
        {
            Debug.Log("You said:" + args.text);
            _keywordAction.Invoke();
            
        }
    }

    

    private void OnApplicationQuit()
    {
        if (_recognizer != null && _recognizer.IsRunning)
        {
            _recognizer.OnPhraseRecognized -= KeywordRecognizer_OnPhraseRecognized;
            _recognizer.Stop();
        }
    }
    
    
}