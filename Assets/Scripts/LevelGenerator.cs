﻿using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(Terrain))]
public class LevelGenerator : MonoBehaviour
{
    [SerializeField]
    private bool _generate = false;

    [Header("Size")]
    [SerializeField]
    private int _size = 512;

    [SerializeField]
    private int _height = 256;


    [Header("Noise")]
    [SerializeField]
    private float _scale = 10f;

    [SerializeField]
    private float _offsetX = 0f;

    [SerializeField]
    private float _offsetZ = 0f;

    [SerializeField]
    private float _threshold = 0.5f;

    private Terrain _terrain;
    private TerrainData _terrainData;

    private void Awake()
    {
        _terrain = GetComponent<Terrain>();
        _terrainData = _terrain.terrainData;
    }

    private void Update()
    {
        if (_generate == false) return;

        GenerateTerrain();
    }

    private void GenerateTerrain()
    {
        _terrainData.size = new Vector3(_size, _height, _size);
        _terrainData.heightmapResolution = _size + 1;
        _terrainData.SetHeights(0, 0, GenerateHeights());
    }


    private float[,] GenerateHeights()
    {
        float[,] heights = new float[_size, _size];

        for (int z = 0; z < _size; z++)
        {
            for (int x = 0; x < _size; x++)
            {
                heights[z, x] = GetPerlinHeightAt(x, z);
            }
        }

        return heights;
    }

    private float GetPerlinHeightAt(float x, float z)
    {
        var xCoord = x / (float)_size * _scale + _offsetX;
        var zCoord = z / (float)_size * _scale + _offsetZ;

        var myHeight = Mathf.PerlinNoise(xCoord, zCoord);
        if (myHeight <= _threshold)
        {
            myHeight = 0f;
        }
        else
        {
            myHeight = 1f;
        }

        return myHeight;
    }

}
