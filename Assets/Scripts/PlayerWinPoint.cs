﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWinPoint : MonoBehaviour
{
    [SerializeField]
    GameObject WinScreen;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            WinScreen.SetActive(true);
        }
    }
}
