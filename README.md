Jonathan and Joel. PG18 

The objective of the game is to reach the blue sphere, avoid the edges and the holes in the way.

To move you character you need to use voice commands. 

To go foward in the direction the player is facing say: GO

To turn the character say: Left, Mid-Left, Right, Mid-right or Back.
